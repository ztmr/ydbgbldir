
Usage:

```
IMG=ydb-local-dbg-build
ydb-source-dir$ docker build --build-arg CMAKE_BUILD_TYPE=Debug -t ${IMG} .
this-repo-dir$ ./test.sh --build-arg BASEIMG=${IMG}
```

NOTE: that this is not meant to be a classic test-suite which can fail
if something did not work. This is only meant to observe the changes
made in YottaDB code.
