xlate
 N $ET S $ET="W !,""ERROR: "",$ZSTATUS,!! S ($EC,$ZS)="""" ZG $ZL-1"
 D print()
 W "Let's set $ZGBLDIR directly...",!
 S $ZGBLDIR=$ZTRNLNM("ydb_gbldir")
 D print()
 W "Try to browse %GTMHELP globals:",!
 D listgbls("^[""%GTMHELP""]%")
 W "Try to browse %GTMHELP globals forcibly retaining the extended reference:",!
 D listgbls("^[""%GTMHELP""]%",1)
 W !!
 W $G(^["%GTMHELP"]Test)
 W "Is current $Reference='^[""%GTMHELP""]Test'?",!
 W "           $Reference='",$Reference,"'",!!!
 Q
print()
 W "-----------------------------------------",!
 W "$ZGBLDIR           = ",$ZGBLDIR,!
 W "getenv(ydb_gbldir) = ",$ZTRNLNM("ydb_gbldir"),!
 W "getenv(gtmgbldir)  = ",$ZTRNLNM("gtmgbldir"),!
 W "-----------------------------------------",!
 W "Globals:",! D listgbls()
 W "-----------------------------------------",!
 Q
listgbls(g,f)
 N r S g=$G(g,"^%"),f=+$G(f),r=g
 W "Starting with g="_g,!
 F  S g=$O(@g) Q:g=""  D
 . S:f&(r["^[") $E(g,1)=("^["_$P($P(r,"[",2),"]",1)_"]")
 . W g," ",!
 W !
 Q
