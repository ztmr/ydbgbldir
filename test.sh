#!/bin/sh
IMG=ydb-test-$$
docker build --no-cache $@ -t ${IMG} $(dirname $0)
docker rmi ${IMG}
