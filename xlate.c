#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "libyottadb.h"

#define DBG_LOG(fmt, ...) fprintf (stderr, "\n" "XLATEDBG: " fmt "\n", __VA_ARGS__)

#define PREFIX_STR "$ydb_dist/"
#define PREFIX_LEN 10
#define YDB_MALLOC malloc   // XXX: for simplicity

static char *  g_err_malloc = "Unable to allocate memory.";

int ydb_generic_xlate_internal (const char * call_type,
                                ydb_string_t *in1,
                                ydb_string_t *in2,
                                ydb_string_t *zdir,
                                ydb_string_t *res);

#if XLATE_GLDENV_SHARED > 0
int ydb_gbldir_xlate (ydb_string_t *in1,
                      ydb_string_t *in2,
                      ydb_string_t *zdir,
                      ydb_string_t *res)
    __attribute__ ((alias ("ydb_generic_xlate")));

int ydb_env_xlate (ydb_string_t *in1,
                   ydb_string_t *in2,
                   ydb_string_t *zdir,
                   ydb_string_t *res)
    __attribute__ ((alias ("ydb_generic_xlate")));

int ydb_generic_xlate (ydb_string_t *in1,
                       ydb_string_t *in2,
                       ydb_string_t *zdir,
                       ydb_string_t *res) {
  return ydb_generic_xlate_internal ("GLDENV", in1, in2, zdir, res);
}
#else
int ydb_gbldir_xlate (ydb_string_t *in1,
                      ydb_string_t *in2,
                      ydb_string_t *zdir,
                      ydb_string_t *res) {
  return ydb_generic_xlate_internal ("GBLDIR", in1, in2, zdir, res);
}

int ydb_env_xlate (ydb_string_t *in1,
                   ydb_string_t *in2,
                   ydb_string_t *zdir,
                   ydb_string_t *res) {
  return ydb_generic_xlate_internal ("ENVIRN", in1, in2, zdir, res);
}
#endif

int ydb_generic_xlate_internal (const char * call_type,
                                ydb_string_t *in1,
                                ydb_string_t *in2,
                                ydb_string_t *zdir,
                                ydb_string_t *res) {

  DBG_LOG ("Call: %s UCI=[%.*s,%.*s] $ZDIR=(%.*s)",
           call_type,
           (int) in1->length, in1->address,
           (int) in2->length, in2->address,
           (int) zdir->length, zdir->address);

  if (*(in1->address) != '%') {
    res->address = in1->address;
    res->length  = in1->length;
    DBG_LOG ("Retaining the original: %.*s",
             (int) res->length, res->address);
    return 0;
  }

  // "%" + <something> => "$ydb_dist/" + lowerCase(<something>)
  // no need to append .gld since ydb does that automagically
  res->length  = (unsigned long) (in1->length + PREFIX_LEN -1);
  if ((res->address = (char *) YDB_MALLOC (res->length)) == NULL) {
    DBG_LOG ("%s", g_err_malloc);
    res->length = strlen (g_err_malloc);
    memcpy (res->address, g_err_malloc, res->length);
    return 1;
  }

  memcpy (res->address, PREFIX_STR, (size_t) PREFIX_LEN);
  int i;
  for (i = 0; i < in1->length - 1; i++)
    res->address [PREFIX_LEN+i] = tolower(in1->address [i+1]);

  DBG_LOG ("Res: [%.*s] (len=%ld)",
           (int) res->length, res->address,
           res->length);

  return 0;
}
