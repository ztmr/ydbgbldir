ARG BASEIMG=yottadb/yottadb-debug:r1.29
FROM ${BASEIMG}
ARG XLATE_GLDENV_SHARED=1
COPY xlate.* /data/
# Ensure the right workdir (should be inherited from base image)
WORKDIR /data
ENV ydb_env_translate=/data/xlate.so \
    ydb_gbldir_translate=/data/xlate.so \
    ydb_dbglvl=8388608 \
    ydb_routines="/data /opt/yottadb/current/libyottadbutil.so"
RUN gcc xlate.c -o xlate.so -DXLATE_GLDENV_SHARED=${XLATE_GLDENV_SHARED} \
                $(pkg-config --libs --cflags yottadb) -fPIC -shared \
 && env ydb_gbldir="%DSEHELP" /opt/yottadb/current/mumps -run xlate \
 && echo "*** DSE" \
 && env ydb_gbldir="%DSEHELP" /opt/yottadb/current/dse dump -fi \
 && echo "*** GDE" \
# && env ydb_gbldir="%NEWGBLDIR" /opt/yottadb/current/mumps -run GDE @xlate.gde \
 && env ydb_gbldir="/opt/yottadb/current/newgbldir" /opt/yottadb/current/mumps -run GDE @xlate.gde \
 && echo "*** MUPIP" \
 && env ydb_gbldir="%NEWGBLDIR" /opt/yottadb/current/mupip create \
 && echo "*** LKE" \
 && env ydb_gbldir="%NEWGBLDIR" /opt/yottadb/current/lke show
